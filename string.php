<?php
   $title = "Latih PHP";
   ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo $title ?></title>
</head>
<body>
    <h2>Latih PHP<h2>
     <?php
         echo "<h3>Soal 1</h3>";
         $kalimat ="PHP is never old";
         echo "kalimat 1 :  ".$kalimat . "<br>";
         echo "Panjang String : " . strlen($kalimat) . "<br>";
         echo "Jumlah Kata : " . str_word_count($kalimat) . "<br>";

         echo "<h3>Soal 2</h3>";
         $kalimat2 = "I Love PHP";
         echo "kalimat : " . $kalimat2 ."<br>";
         echo "kata 2 : " . substr($kalimat2,0,1). "<br>";
         echo "kata 3 : " . substr($kalimat2,7,3). "<br>";

         echo "<h3>Soal 3</h3>";
         $kalimat3 = "PHP is old but Good!";
         echo "Kalimat 3 : " . $kalimat3 . "<br>";
         echo "Kalimat 3 di ubah : ". str_replace("Good!","awesome",$kalimat3);


      ?>
</body>
</html>